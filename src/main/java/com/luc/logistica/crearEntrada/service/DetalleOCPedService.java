package com.luc.logistica.crearEntrada.service;

import java.util.List;
import com.luc.logistica.crearEntrada.model.DetalleOCPed;

public interface DetalleOCPedService {
	public DetalleOCPed findByPed(Integer numero, String tipo, String producto);
	public List<DetalleOCPed> findByDetalle(Integer numero, String tipo);
	public List<DetalleOCPed> findAll();
	public DetalleOCPed saveDetOCPed(DetalleOCPed docPed);
}