package com.luc.logistica.crearEntrada.service;

import java.util.List;
import com.luc.logistica.crearEntrada.model.EncabezadoOC;

public interface OrdenCompraService {
	public EncabezadoOC findById(Integer numero, String tipo);
	public EncabezadoOC findByOper(String operacion);
	public List<EncabezadoOC> findAll();
	public void saveOC(EncabezadoOC oc);
	public List<EncabezadoOC> findFinalizado();
}
