package com.luc.logistica.crearEntrada.service;

import java.util.List;
import com.luc.logistica.crearEntrada.model.DetalleOC;

public interface DetalleOCService {
	public DetalleOC findById(Integer numero);

	public Object findByPromVenta(String tipo, String codigo, Integer anio, Integer mes);
	public Object findByCount(String tipo, String codigo, Integer anio, Integer mes);
	//public List<DetalleOC> findByPromVenta(String tipo, String codigo);

	public List<DetalleOC> findAll();

	public void saveDetOC(DetalleOC doc);
}
