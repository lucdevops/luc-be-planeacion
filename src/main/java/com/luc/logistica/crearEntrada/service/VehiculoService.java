package com.luc.logistica.crearEntrada.service;

import java.util.List;
import com.luc.logistica.crearEntrada.model.Vehiculo;

public interface VehiculoService {

	public Vehiculo findById(String placa);

	public List<Vehiculo> findAll();

	public Vehiculo save(Vehiculo vehiculo);
}
