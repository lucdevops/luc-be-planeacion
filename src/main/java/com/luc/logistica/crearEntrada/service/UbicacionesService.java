package com.luc.logistica.crearEntrada.service;

import java.util.List;

import com.luc.logistica.crearEntrada.model.Ubicaciones;;

public interface UbicacionesService {
	public Ubicaciones findByUbi(String ubicacion);
	public List<Ubicaciones> findAll();
	public Ubicaciones save(Ubicaciones ubicacion);
}
