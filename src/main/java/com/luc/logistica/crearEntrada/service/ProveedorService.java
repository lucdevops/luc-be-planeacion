package com.luc.logistica.crearEntrada.service;

import java.util.List;
import com.luc.logistica.crearEntrada.model.Proveedor;

public interface ProveedorService {
	public Proveedor findById(String id);
	public Proveedor findByName(String nombre);
	public Proveedor findByBodega(String bodega);
	public List<Proveedor> findAll();
}
