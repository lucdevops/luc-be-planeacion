package com.luc.logistica.crearEntrada.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.luc.logistica.crearEntrada.model.Lote;
import com.luc.logistica.crearEntrada.repository.LoteRepository;
import com.luc.logistica.crearEntrada.service.LoteService;

@Service("loteService")
public class LoteServiceImpl implements LoteService {

	@Autowired
	private LoteRepository loteRepo;

	@Override
	public List<Lote> findByOrc(String numero){
		return loteRepo.findByOrc(numero);
	}

	@Override
	public List<Lote> findAll() {
		return loteRepo.findAll();
	}

	@Override
	public Lote saveLote(Lote lt) {
		loteRepo.flush();
		try {
			return loteRepo.save(lt);
		}catch(Exception e) {
			e.printStackTrace();
		}	
		return null;
	}

	@Override
	public Lote findByLote(String codigo, String lote) {
		return loteRepo.findByLote(codigo,lote);
	}

}