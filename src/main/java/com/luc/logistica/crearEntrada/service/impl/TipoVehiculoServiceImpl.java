package com.luc.logistica.crearEntrada.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.luc.logistica.crearEntrada.model.TipoVehiculo;
import com.luc.logistica.crearEntrada.repository.TipoVehiculoRepository;
import com.luc.logistica.crearEntrada.service.TipoVehiculoService;;

@Service("tipovehiculoService")
public class TipoVehiculoServiceImpl implements TipoVehiculoService {

    @Autowired
    private TipoVehiculoRepository tipovehiculoRepository;

    @Override
    public TipoVehiculo findByNombre(String nombreVehiculo) {
        return tipovehiculoRepository.findByNombre(nombreVehiculo);
    }

    @Override
    public List<TipoVehiculo> findAll() {
        return tipovehiculoRepository.findAll();
    }

    @Override
    public TipoVehiculo save(TipoVehiculo tipovehiculo) {
        return tipovehiculoRepository.save(tipovehiculo);
    }

    @Override
    public TipoVehiculo findById(Integer idVehiculo) {
        return tipovehiculoRepository.findById(idVehiculo);
    }
}
