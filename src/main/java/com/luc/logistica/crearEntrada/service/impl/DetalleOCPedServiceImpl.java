package com.luc.logistica.crearEntrada.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.luc.logistica.crearEntrada.model.DetalleOCPed;
import com.luc.logistica.crearEntrada.repository.DetalleOCPedRepository;
import com.luc.logistica.crearEntrada.service.DetalleOCPedService;

@Service("detOrdenCompraPedService")

public class DetalleOCPedServiceImpl implements DetalleOCPedService {

	@Autowired
	private DetalleOCPedRepository docPedRepo;

	@Override
	public List<DetalleOCPed> findAll() {
		return null;
	}

	@Override
	public DetalleOCPed saveDetOCPed(DetalleOCPed docPed) {
		docPedRepo.flush();
		try {
			return docPedRepo.save(docPed);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return null;
		
	}

	@Override
	public DetalleOCPed findByPed(Integer numero, String tipo, String producto) {
		return docPedRepo.findByPed(numero, tipo, producto);
	}

	@Override
	public List<DetalleOCPed> findByDetalle(Integer numero, String tipo) {
		return docPedRepo.findByDetalle(numero, tipo);
	}
	
}