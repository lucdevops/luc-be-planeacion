package com.luc.logistica.crearEntrada.service;

import java.util.List;

import com.luc.logistica.crearEntrada.model.TipoVehiculo;

public interface TipoVehiculoService {
	public TipoVehiculo findByNombre(String nombreVehiculo);
	public TipoVehiculo findById(Integer idVehiculo);
	public List<TipoVehiculo> findAll();
	public TipoVehiculo save(TipoVehiculo tipovehiculo);
}
