package com.luc.logistica.crearEntrada.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.luc.logistica.crearEntrada.model.ClaseVehiculo;

@Repository("clasevehiculoRepository")
public interface ClaseVehiculoRepository extends JpaRepository<ClaseVehiculo, Integer> {
	
	 @Query("select c from ClaseVehiculo c where c.nombreClaseV = ?1")
	 ClaseVehiculo findByNombre(String nombreClaseV);

	 @Query("select c from ClaseVehiculo c where c.idClase = ?1")
	 ClaseVehiculo findById(Integer idClase);
	
	
}
