package com.luc.logistica.crearEntrada.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.luc.logistica.crearEntrada.model.Meses;

@Repository("mesesRepository")
public interface MesesRepository extends JpaRepository<Meses, String> {
	
	 @Query("SELECT me FROM Meses me WHERE me.mes = ?1")
	 Meses findByMes(String mes);
	
}