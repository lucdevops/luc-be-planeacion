package com.luc.logistica.crearEntrada.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

import com.luc.logistica.crearEntrada.model.Um;

@Repository("umRepository")
public interface UmRepository extends JpaRepository<Um, Integer> {

	@Query("select u from Um u where u.codigo = ?1")
	List<Um> findById(String codigo);

	@Query("select u from Um u where u.codigo = ?1 and u.org = ?2")
	Um findByUndMin(String codigo, String unidad);
}