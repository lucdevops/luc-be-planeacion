package com.luc.logistica.crearEntrada.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

import com.luc.logistica.crearEntrada.model.Estimado;
import com.luc.logistica.crearEntrada.model.EstimadoKey;


@Repository("estimadoRepository")
public interface EstimadoRepository extends JpaRepository<Estimado, EstimadoKey> {

    @Query("select est from Estimado est where est.llave.ano = ?1 AND est.llave.mes = ?2 AND est.llave.tipo = ?3 AND est.llave.cliente.nit = ?4 AND est.llave.referencia.codigo = ?5")
    Estimado findByEst(Integer ano, Integer mes, String tipo, Double cliente, String referencia);

    @Query("select e.llave.tipo, e.llave.ano, e.llave.mes, e.llave.referencia.marca.id, e.llave.referencia.marca.nombre, ROUND(sum(e.estimado) / 1000,0) as pron from Estimado e  where e.llave.ano = ?1 and e.llave.mes = ?2 GROUP BY e.llave.tipo,e.llave.ano,e.llave.mes,e.llave.referencia.marca.id,e.llave.referencia.marca.nombre")
    List<Object> findByPeriodoMarca(Integer ano, Integer mes);

    @Query("select e.llave.tipo, e.llave.ano, e.llave.mes, e.llave.referencia.codigo, r.descripcion, r.tipo, rt.dias, sum(e.cantidad) from Estimado e, Referencias r, ReferenciasTipo rt where r.marca.id = ?1 and e.llave.mes = ?2 and e.llave.referencia.codigo = r.codigo and r.tipo = rt.tipo group by e.llave.tipo, e.llave.ano, e.llave.mes, e.llave.referencia.codigo, r.descripcion, r.tipo, rt.dias ORDER BY r.tipo")
    //@Query("select e.llave.tipo, e.llave.ano, e.llave.mes, e.llave.referencia.codigo, sum(e.cantidad) from Estimado e, Referencias r, ReferenciasTipo rt where r.marca.id = ?1 and e.llave.mes = ?2 and e.llave.referencia.codigo = r.codigo and r.tipo = rt.tipo group by e.llave.tipo, e.llave.ano, e.llave.mes, e.llave.referencia.codigo")
    List<Object[]> findPrueba(String marca, Integer mes);

    @Query("select e.llave.referencia.codigo as codigo, r.descripcion as descripcion, r.tipo as rTipo, rt.dias as dias, sum(e.cantidad) as cantidad from Estimado e, Referencias r, ReferenciasTipo rt where e.llave.mes = ?1 and r.codigo = ?2 and e.llave.referencia.codigo = r.codigo and r.tipo = rt.tipo group by e.llave.referencia.codigo, r.descripcion, r.tipo, rt.dias ORDER BY r.tipo")
    List<Object[]> findEstRef(Integer mes, String codigo);

    @Query("select sum(pu.cntEntrada - pu.cntSalida) as saldo from Ubicaciones u, ProdUbicado pu, Referencias r where r.codigo = ?1 and u.id = pu.idUbi and r.id = pu.idRefe and (pu.cntEntrada - pu.cntSalida) != 0")
    Double findStockActual(String codigo);
}