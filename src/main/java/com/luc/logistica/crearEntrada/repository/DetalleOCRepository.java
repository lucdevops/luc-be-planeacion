package com.luc.logistica.crearEntrada.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

import com.luc.logistica.crearEntrada.model.DetalleOC;

@Repository("detalleOCRepository")
public interface DetalleOCRepository extends JpaRepository<DetalleOC, Integer> {

	@Query("select doc from DetalleOC doc where doc.id = ?1")
	DetalleOC findById(Integer id);

	@Query("select SUM(doc.cantidad) from DetalleOC doc where doc.tipo = ?1 and doc.refe.codigo = ?2 and YEAR(doc.fecha) = ?3 and MONTH(doc.fecha) = ?4")
	Object findByPromVenta(String tipo, String codigo, Integer anio, Integer mes);

	@Query("select COUNT(doc) from DetalleOC doc where doc.tipo = ?1 and doc.refe.codigo = ?2 and YEAR(doc.fecha) = ?3 and MONTH(doc.fecha) = ?4")
	Object findByCount(String tipo, String codigo, Integer anio, Integer mes);

	/*@Query("select doc from DetalleOC doc where doc.tipo = ?1 and doc.refe.codigo = ?2")
	List<DetalleOC> findByPromVenta(String tipo, String codigo);*/

}