package com.luc.logistica.crearEntrada.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.luc.logistica.crearEntrada.model.Ubicaciones;;

@Repository("ubicacionesRepository")
public interface UbicacionesRepository extends JpaRepository<Ubicaciones, Integer> {
	
	 @Query("select u from Ubicaciones u where u.ubicacion = ?1")
	 Ubicaciones findByUbi(String ubicacion);
	 
	
}