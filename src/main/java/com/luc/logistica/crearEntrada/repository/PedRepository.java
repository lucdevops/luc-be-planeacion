package com.luc.logistica.crearEntrada.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.luc.logistica.crearEntrada.model.Ped;
import com.luc.logistica.crearEntrada.model.PedKey;

import java.util.Date;
import java.util.List;

@Repository("pedRepository")
public interface PedRepository extends JpaRepository<Ped, PedKey> {

	@Query("select pe from Ped pe where pe.pedKey.numero = ?1 and pe.tipo = ?2")
	Ped findByPed(Integer numero, String tipo);

	@Query("select pe from Ped pe where pe.so_id = ?1")
	Ped findById(String id);

	@Query("select pe from Ped pe where pe.proveedor = ?1 and pe.estado != 'FI' and pe.fecha between ?2 and ?3")
	List<Ped> findExistente(String proveedor, Date back, Date today);

	@Query("SELECT pe from Ped pe WHERE (pe.tipo = 'ORC' AND pe.estado = 'RE') OR (pe.tipo = 'OPP' AND pe.estado = 'RE')")
	List<Ped> findEnProceso();

	@Query("SELECT pe from Ped pe WHERE (pe.tipo = 'ORC' AND pe.estado = 'PE') OR (pe.tipo = 'OPP' AND pe.estado = 'PE')")
	List<Ped> findEnTransito();

	/*
	 * @Query("SELECT pe from Ped pe WHERE DATEPART(dd,pe.fecha) = ?1 AND DATEPART(mm,pe.fecha) = ?2 AND DATEPART(yyyy,pe.fecha) = ?3"
	 * ) List<Ped> findCliAgen(String dia, String mes, String anio);
	 */

	@Query("SELECT pe from Ped pe WHERE pe.proveedor = ?1 AND pe.estado = NULL")
	List<Ped> findCliAgen(String proveedor);
}
