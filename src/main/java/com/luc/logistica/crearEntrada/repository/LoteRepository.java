package com.luc.logistica.crearEntrada.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

import com.luc.logistica.crearEntrada.model.Lote;

@Repository("loteRepository")
public interface LoteRepository extends JpaRepository<Lote, Integer> {
	
	@Query("select l from Lote l where l.codigo = ?1 AND l.lote = ?2")
	Lote findByLote(String codigo, String lote);

	@Query("select l from Lote l where l.numero = ?1")
	List<Lote> findByOrc(String numero);
}