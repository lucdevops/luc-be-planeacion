package com.luc.logistica.crearEntrada.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.luc.logistica.crearEntrada.model.PedHis;
import com.luc.logistica.crearEntrada.model.PedKey;

@Repository("pedHisRepository")
public interface PedHisRepository extends JpaRepository<PedHis, PedKey> {

}
