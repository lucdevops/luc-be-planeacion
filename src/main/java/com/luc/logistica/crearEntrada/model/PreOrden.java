package com.luc.logistica.crearEntrada.model;

public class PreOrden {
    private String tipo;
    private Integer anio;
    private Integer mes;
    private String codigo;
    private String descripcion;
    private Marca marca;
    private Integer timeRepo;
    private Integer leadTime;
    private String tipoRef;
    private Integer diasInv;
    private Integer cntEstimado;
    private Double stock;

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Integer getAnio() {
        return anio;
    }

    public void setAnio(Integer anio) {
        this.anio = anio;
    }

    public Integer getMes() {
        return mes;
    }

    public void setMes(Integer mes) {
        this.mes = mes;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Marca getMarca() {
        return marca;
    }

    public void setMarca(Marca marca) {
        this.marca = marca;
    }

    public Integer getTimeRepo() {
        return timeRepo;
    }

    public void setTimeRepo(Integer timeRepo) {
        this.timeRepo = timeRepo;
    }

    public Integer getLeadTime() {
        return leadTime;
    }

    public void setLeadTime(Integer leadTime) {
        this.leadTime = leadTime;
    }

    public String getTipoRef() {
        return tipoRef;
    }

    public void setTipoRef(String tipoRef) {
        this.tipoRef = tipoRef;
    }

    public Integer getCntEstimado() {
        return cntEstimado;
    }

    public void setCntEstimado(Integer cntEstimado) {
        this.cntEstimado = cntEstimado;
    }

    public Double getStock() {
        return stock;
    }

    public void setStock(Double stock) {
        this.stock = stock;
    }

    public Integer getDiasInv() {
        return diasInv;
    }

    public void setDiasInv(Integer diasInv) {
        this.diasInv = diasInv;
    }

}
