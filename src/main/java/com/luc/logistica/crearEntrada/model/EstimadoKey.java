package com.luc.logistica.crearEntrada.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Embeddable
public class EstimadoKey implements Serializable {

	@Column(name = "fc_ano")
	private Integer ano;
	@Column(name = "fc_mes")
	private Integer mes;
	@Column(name = "fc_tipo")
	private String tipo;
	@ManyToOne
	@JoinColumn(name = "fc_cliente")
	private Terceros cliente;
	@ManyToOne
	@JoinColumn(name = "fc_refcodigo")
	private Referencias referencia;

	public EstimadoKey(Integer ano, Integer mes, String tipo, Terceros cliente, Referencias referencia) {
		super();
		this.ano = ano;
		this.mes = mes;
		this.tipo = tipo;
		this.cliente = cliente;
		this.referencia = referencia;
	}

	public Integer getAno() {
		return ano;
	}

	public void setAno(Integer ano) {
		this.ano = ano;
	}

	public Integer getMes() {
		return mes;
	}

	public void setMes(Integer mes) {
		this.mes = mes;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Terceros getCliente() {
		return cliente;
	}

	public void setCliente(Terceros cliente) {
		this.cliente = cliente;
	}

	public Referencias getReferencia() {
		return referencia;
	}

	public void setReferencia(Referencias referencia) {
		this.referencia = referencia;
	}

	public EstimadoKey() {
	}

}
