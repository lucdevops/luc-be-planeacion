package com.luc.logistica.crearEntrada.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tipo_vehiculo_mstr")
public class TipoVehiculo {


    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_tipo_vehiculo",nullable = false, unique = true)
    private Integer idVehiculo;
    
    @Column(name = "nom_tipo_vehiculo")
    private String nombreVehiculo;

    public Integer getIdVehiculo() {
        return idVehiculo;
    }

    public void setIdVehiculo(Integer idVehiculo) {
        this.idVehiculo = idVehiculo;
    }

    public String getNombreVehiculo() {
        return nombreVehiculo;
    }

    public void setNombreVehiculo(String nombreVehiculo) {
        this.nombreVehiculo = nombreVehiculo;
    }

	public List<TipoVehiculo> findAll() {
		return null;
	}
}
