package com.luc.logistica.crearEntrada.model;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "fc_hist")
public class Estimado {
	@EmbeddedId
	protected EstimadoKey llave;
	@Column(name = "fc_estimado")
	private Double estimado;
	@Column(name = "fc_qty")
	private Integer cantidad;
	@Column(name = "fc_pp")
	private Double precio;
	@Column(name = "fc_qty_day")
	private Double cantidadDia;

	public EstimadoKey getLlave() {
		return llave;
	}

	public void setLlave(EstimadoKey llave) {
		this.llave = llave;
	}

	public Double getEstimado() {
		return estimado;
	}

	public void setEstimado(Double estimado) {
		this.estimado = estimado;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public Double getPrecio() {
		return precio;
	}

	public void setPrecio(Double precio) {
		this.precio = precio;
	}

	public Double getCantidadDia() {
		return cantidadDia;
	}

	public void setCantidadDia(Double cantidadDia) {
		this.cantidadDia = cantidadDia;
	}

}
