package com.luc.logistica.crearEntrada.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "terceros")
public class Terceros {

	@Id
	@Column(name = "nit", columnDefinition = "DECIMAL")
	private Double nit;
	@Column(name = "ean")
	private String ean;
	@Column(name = "nombres")
	private String nombres;
	@Column(name = "codigo_luc")
	private String codigoLuc;
	@Column(name = "list_precio")
	private String listaPrecio;
	@Column(name = "ciudad")
	private String ciudad;
	@Column(name = "direccion")
	private String direccion;
	@Column(name = "nit_real")
	private Double nitReal;
	@Column(name = "vendedor")
	private Double vendedor;
	@Column(name = "vd")
	private String vd;
	@Column(name = "pais")
	private String pais;
	@Column(name = "gran_contribuyente")
	private Integer contribuyente;
	@Column(name = "autoretenedor")
	private Integer autoretenedor;
	@Column(name = "concepto_1")
	private String concep1;
	@Column(name = "concepto_6")
	private String concep6;
	@Column(name = "y_pais")
	private String ypais;
	@Column(name = "y_dpto")
	private Integer ydpto;
	@Column(name = "y_ciudad")
	private Integer yciudad;
	@Column(name = "dia_recibo")
	private String diaRecibo;
	@Column(name = "id")
	private String id;
	@Column(name = "prov_rey")
	private String provRey;
	@Column(name = "prov_triguisar")
	private String provTriguisar;
	@Column(name = "prov_chefrito")
	private String provChefrito;
	@Column(name = "prov_gamba")
	private String provGamba;
	@Column(name = "prov_ales")
	private String provAles;
	@Column(name = "prov_frutalia")
	private String provFrutalia;
	@Column(name = "prov_itex")
	private String provItex;
	@Column(name = "prov_katori")
	private String provKatori;
	@Column(name = "prov_philips")
	private String provPhilips;
	@Column(name = "prov_vitaave")
	private String provVitaave;
	@Column(name = "prov_sakurahana")
	private String provSakurahana;
	@Column(name = "prov_fregona")
	private String provFregona;
	@Column(name = "prov_rosmi")
	private String provRosmi;
	@Column(name = "prov_intear")
	private String provIntear;
	@Column(name = "prov_ecofaro")
	private String provEcofaro;
	@Column(name = "prov_celema")
	private String provCelema;
	@Column(name = "prov_ripals")
	private String provRipals;

	public String getCodigoLuc() {
		return codigoLuc;
	}

	public void setCodigoLuc(String codigoLuc) {
		this.codigoLuc = codigoLuc;
	}

	public Double getNit() {
		return nit;
	}

	public void setNit(Double nit) {
		this.nit = nit;
	}

	public String getEan() {
		return ean;
	}

	public void setEan(String ean) {
		this.ean = ean;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getListaPrecio() {
		return listaPrecio;
	}

	public void setListaPrecio(String listaPrecio) {
		this.listaPrecio = listaPrecio;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public Double getNitReal() {
		return nitReal;
	}

	public void setNitReal(Double nitReal) {
		this.nitReal = nitReal;
	}

	public Double getVendedor() {
		return vendedor;
	}

	public void setVendedor(Double vendedor) {
		this.vendedor = vendedor;
	}

	public String getVd() {
		return vd;
	}

	public void setVd(String vd) {
		this.vd = vd;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public Integer getContribuyente() {
		return contribuyente;
	}

	public void setContribuyente(Integer contribuyente) {
		this.contribuyente = contribuyente;
	}

	public Integer getAutoretenedor() {
		return autoretenedor;
	}

	public void setAutoretenedor(Integer autoretenedor) {
		this.autoretenedor = autoretenedor;
	}

	public String getConcep1() {
		return concep1;
	}

	public void setConcep1(String concep1) {
		this.concep1 = concep1;
	}

	public String getConcep6() {
		return concep6;
	}

	public void setConcep6(String concep6) {
		this.concep6 = concep6;
	}

	public String getYpais() {
		return ypais;
	}

	public void setYpais(String ypais) {
		this.ypais = ypais;
	}

	public Integer getYdpto() {
		return ydpto;
	}

	public void setYdpto(Integer ydpto) {
		this.ydpto = ydpto;
	}

	public Integer getYciudad() {
		return yciudad;
	}

	public void setYciudad(Integer yciudad) {
		this.yciudad = yciudad;
	}

	public String getDiaRecibo() {
		return diaRecibo;
	}

	public void setDiaRecibo(String diaRecibo) {
		this.diaRecibo = diaRecibo;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getProvRey() {
		return provRey;
	}

	public void setProvRey(String provRey) {
		this.provRey = provRey;
	}

	public String getProvTriguisar() {
		return provTriguisar;
	}

	public void setProvTriguisar(String provTriguisar) {
		this.provTriguisar = provTriguisar;
	}

	public String getProvChefrito() {
		return provChefrito;
	}

	public void setProvChefrito(String provChefrito) {
		this.provChefrito = provChefrito;
	}

	public String getProvGamba() {
		return provGamba;
	}

	public void setProvGamba(String provGamba) {
		this.provGamba = provGamba;
	}

	public String getProvAles() {
		return provAles;
	}

	public void setProvAles(String provAles) {
		this.provAles = provAles;
	}

	public String getProvFrutalia() {
		return provFrutalia;
	}

	public void setProvFrutalia(String provFrutalia) {
		this.provFrutalia = provFrutalia;
	}

	public String getProvItex() {
		return provItex;
	}

	public void setProvItex(String provItex) {
		this.provItex = provItex;
	}

	public String getProvKatori() {
		return provKatori;
	}

	public void setProvKatori(String provKatori) {
		this.provKatori = provKatori;
	}

	public String getProvPhilips() {
		return provPhilips;
	}

	public void setProvPhilips(String provPhilips) {
		this.provPhilips = provPhilips;
	}

	public String getProvVitaave() {
		return provVitaave;
	}

	public void setProvVitaave(String provVitaave) {
		this.provVitaave = provVitaave;
	}

	public String getProvSakurahana() {
		return provSakurahana;
	}

	public void setProvSakurahana(String provSakurahana) {
		this.provSakurahana = provSakurahana;
	}

	public String getProvFregona() {
		return provFregona;
	}

	public void setProvFregona(String provFregona) {
		this.provFregona = provFregona;
	}

	public String getProvRosmi() {
		return provRosmi;
	}

	public void setProvRosmi(String provRosmi) {
		this.provRosmi = provRosmi;
	}

	public String getProvIntear() {
		return provIntear;
	}

	public void setProvIntear(String provIntear) {
		this.provIntear = provIntear;
	}

	public String getProvEcofaro() {
		return provEcofaro;
	}

	public void setProvEcofaro(String provEcofaro) {
		this.provEcofaro = provEcofaro;
	}

	public String getProvCelema() {
		return provCelema;
	}

	public void setProvCelema(String provCelema) {
		this.provCelema = provCelema;
	}

	public String getProvRipals() {
		return provRipals;
	}

	public void setProvRipals(String provRipals) {
		this.provRipals = provRipals;
	}

	
	
}
