package com.luc.logistica.crearEntrada.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "referencias_tipo")
public class ReferenciasTipo {
    @Id
    @Column(name = "tipo")
    private String tipo;
    @Column(name = "dias")
    private Integer dias;

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Integer getDias() {
        return dias;
    }

    public void setDias(Integer dias) {
        this.dias = dias;
    }

}
