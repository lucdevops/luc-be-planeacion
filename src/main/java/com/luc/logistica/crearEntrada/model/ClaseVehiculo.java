package com.luc.logistica.crearEntrada.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "clase_vehiculo_mstr")
public class ClaseVehiculo {
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_clase_vehiculo",nullable = false, unique = true)
    private Integer idClase;
    
    @Column(name = "nom_clase_vehiculo")
    private String nombreClaseV;
    
    public Integer getIdClase() {
        return idClase;
    }

    public void setIdClase(Integer idClase) {
        this.idClase = idClase;
    }

    public String getNombreClaseV() {
        return nombreClaseV;
    }

    public void setNombreClaseV(String nombreClaseV) {
        this.nombreClaseV = nombreClaseV;
    }

	



   

}
