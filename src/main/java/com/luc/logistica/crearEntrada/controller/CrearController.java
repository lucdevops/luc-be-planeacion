package com.luc.logistica.crearEntrada.controller;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.luc.logistica.crearEntrada.model.DetalleOC;
import com.luc.logistica.crearEntrada.model.DetalleOCPed;
import com.luc.logistica.crearEntrada.model.DetalleOCPedHis;
import com.luc.logistica.crearEntrada.model.DocKey;
import com.luc.logistica.crearEntrada.model.EncabezadoOC;
import com.luc.logistica.crearEntrada.model.Lote;
import com.luc.logistica.crearEntrada.model.Marca;
import com.luc.logistica.crearEntrada.model.Ped;
import com.luc.logistica.crearEntrada.model.PedHis;
import com.luc.logistica.crearEntrada.model.PedKey;
import com.luc.logistica.crearEntrada.model.Proveedor;
import com.luc.logistica.crearEntrada.model.Referencias;
import com.luc.logistica.crearEntrada.model.ReferenciasTipo;
import com.luc.logistica.crearEntrada.model.Terceros;
import com.luc.logistica.crearEntrada.model.Um;
import com.luc.logistica.crearEntrada.service.DetalleOCPedHisService;
import com.luc.logistica.crearEntrada.service.DetalleOCPedService;
import com.luc.logistica.crearEntrada.service.DetalleOCService;
import com.luc.logistica.crearEntrada.service.LoteService;
import com.luc.logistica.crearEntrada.service.MarcaService;
import com.luc.logistica.crearEntrada.service.OrdenCompraService;
import com.luc.logistica.crearEntrada.service.PedHisService;
import com.luc.logistica.crearEntrada.service.PedService;
import com.luc.logistica.crearEntrada.service.ProveedorService;
import com.luc.logistica.crearEntrada.service.ReferenciasService;
import com.luc.logistica.crearEntrada.service.ReferenciasTipoService;
import com.luc.logistica.crearEntrada.service.TercerosService;
import com.luc.logistica.crearEntrada.service.UmService;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = { "*", "*" })
// @CrossOrigin(origins = { "http://192.168.1.250:8083", "*" })
@RequestMapping("/crear")

public class CrearController {

	@Autowired
	private TercerosService tercerosService;
	@Autowired
	private PedService pedService;
	@Autowired
	private ProveedorService proveedorService;
	@Autowired
	private PedHisService pedHisService;
	@Autowired
	private ReferenciasService refeService;
	@Autowired
	private OrdenCompraService encabezadoService;
	@Autowired
	private DetalleOCPedService docPedService;
	@Autowired
	private DetalleOCPedHisService docPedHisService;
	@Autowired
	private MarcaService marcaService;
	@Autowired
	private ReferenciasTipoService refeTipoService;
	@Autowired
	private LoteService loteOrcService;
	@Autowired
	private UmService umService;
	@Autowired
	private DetalleOCService detOcServices;

	// CREAR UN PEDIDO EN EL WMS
	@GetMapping("/crearPedidoWMS/{agendados}/{proveedor}")
	public boolean crearPedidoWMS(@PathVariable String agendados, @PathVariable String proveedor) {

		boolean registro = false;
		String clientes[] = agendados.split(",");
		Date myDate = new Date();
		SimpleDateFormat mdyFormat = new SimpleDateFormat("MMddyy");
		String fecha = mdyFormat.format(myDate);
		int errores = 0;

		for (int i = 0; i < clientes.length; i++) {

			List<Terceros> tercero = tercerosService.findByTexto(clientes[i]);
			Terceros vend = tercerosService.findByTer(String.valueOf(BigDecimal.valueOf(tercero.get(0).getVendedor())));
			Proveedor prov = proveedorService.findByName(proveedor);
			String prefijo = "LUC";
			String operCP = tercero.get(0).getId().substring(tercero.get(0).getId().length() - 3) + fecha;

			if (prov.getTipo().equals("2")) {
				prefijo = "REP";
			}

			Ped ped = new Ped();
			ped.setAnulado(0);
			ped.setTercero(tercerosService.findByTer(tercero.get(0).getCodigoLuc()));
			ped.setSo_id(operCP);
			ped.setFecha(myDate);
			ped.setFechaHora(myDate);
			ped.setFechaHoraEnt(myDate);
			ped.setNitDestino(tercero.get(0).getNit());
			ped.setPc("API");
			ped.setUsuario("API");
			ped.setVendedor(vend);
			ped.setCondicion("21");
			ped.setCodigoDir(1);
			ped.setCodigoDirDest(1);
			ped.setProveedor(prov.getNombre());
			ped.setTipo(prefijo);
			ped.setPedKey(new PedKey(1, Integer.parseInt(prov.getBodega()), Integer.parseInt(operCP)));
			pedService.savePed(ped);

			PedHis pedHis = new PedHis();
			pedHis.setAnulado(0);
			pedHis.setTercero(tercerosService.findByTer(tercero.get(0).getCodigoLuc()));
			pedHis.setFecha(myDate);
			pedHis.setFechaHora(myDate);
			pedHis.setFechaHoraEnt(myDate);
			pedHis.setNitDestino(tercero.get(0).getNit());
			pedHis.setPc("API");
			pedHis.setUsuario("API");
			pedHis.setVendedor(vend);
			pedHis.setCondicion("21");
			pedHis.setCodigoDir(1);
			pedHis.setProveedor(prov.getNombre());
			pedHis.setTipo(prefijo);
			pedHis.setPedKey(new PedKey(1, Integer.parseInt(prov.getBodega()), Integer.parseInt(operCP)));
			pedHisService.save(pedHis);

			if (pedService.savePed(ped) == null && pedHisService.save(pedHis) == null) {
				errores++;
			}
		}

		if (errores > 0) {
			registro = false;
		} else {
			registro = true;
		}

		return registro;

	}

	// CREAR FACTURA EN WMS (TABLA DOCUMENTOS)
	@GetMapping("/crearFacturaWms/{referencias}/{valorNeto}/{vendedor}/{documento}/{cliente}")
	private void crearFacturaWms(@PathVariable String referencias, @PathVariable Double valorNeto,
			@PathVariable Double vendedor, @PathVariable String documento, @PathVariable String cliente) {

		referencias = "[" + referencias + "]";
		DecimalFormat df = new DecimalFormat("##########");
		String mercaderista = df.format(vendedor);
		JSONArray jsonRef = (JSONArray) JSONValue.parse(referencias);
		Terceros vend = tercerosService.findByTer(mercaderista);
		Terceros ter = tercerosService.findByNit(Double.parseDouble(cliente));
		Date myDate = new Date();

		EncabezadoOC fac = new EncabezadoOC();
		DetalleOC detFac = new DetalleOC();

		fac.setSw(1);
		fac.setTercero(ter);
		fac.setDocKey(new DocKey(Integer.parseInt(documento), "FACV"));
		fac.setFecha(myDate);
		fac.setValorTotal(valorNeto);
		fac.setVendedor(vend);
		fac.setAnulado(false);
		fac.setDocumento(documento);
		fac.setUsuario("API");
		fac.setPc("API");
		fac.setFechaHora(myDate);
		fac.setBodega(1101);
		fac.setPedido(documento);
		fac.setNitDestino(cliente);
		fac.setEstado("FA");

		// falta setiva

		ArrayList<DetalleOC> detalles = new ArrayList<DetalleOC>();

		for (int i = 0; i < jsonRef.size(); i++) {
			JSONObject ref = (JSONObject) jsonRef.get(i);
			int seq = 1;
			Referencias refe = refeService.findByRefe((String) ref.get("irecurso"));
			double precio = Double.parseDouble((String) ref.get("mprecio"));
			double iva = Double.parseDouble((String) ref.get("qporciva"));
			double descuento = Double.parseDouble((String) ref.get("qporcdescuento"));
			double cantidad = Double.parseDouble((String) ref.get("qrecurso"));

			detFac.setBodega(1101);
			detFac.setCantidad(cantidad);
			detFac.setCantidadPedida(cantidad);
			detFac.setDescuento(descuento);
			detFac.setIva(iva);
			detFac.setValor(precio);
			detFac.setRefe(refe);
			detFac.setPedido(documento);
			detFac.setVendedor(vend);
			detFac.setSw(1);
			detFac.setTipo("FACV");
			detFac.setSeq(seq);
			detFac.setTercero(ter);
			detFac.setNumero(Integer.parseInt(documento));
			detalles.add(detFac);

			DetalleOCPed docPed = new DetalleOCPed();
			docPed.setSw(3);
			docPed.setBodega(1101);
			docPed.setNumero(Integer.parseInt(documento));
			docPed.setRefe(refe);
			docPed.setSeq(seq);
			docPed.setCantidad((int) cantidad);
			docPed.setValorUni(precio);
			docPed.setPorcentajeIva(iva);
			docPed.setPorcentajeDes(descuento);
			docPed.setUnd("UND");
			docPed.setCantidadUnd(1.0);
			docPed.setTipo("FACV");
			docPedService.saveDetOCPed(docPed);

			DetalleOCPedHis docPedHis = new DetalleOCPedHis();
			docPedHis.setSw(1);
			docPedHis.setBodega(1101);
			docPedHis.setNumero(Integer.parseInt(documento));
			docPedHis.setCodigo((String) ref.get("irecurso"));
			docPedHis.setSeq(seq);
			docPedHis.setCantidad(cantidad);
			docPedHis.setCantidadDes(0.0);
			docPedHis.setValorUni(precio);
			docPedHis.setPorcentajeIva(iva);
			docPedHis.setPorcentajeDes(descuento);
			docPedHis.setUnd("UND");
			docPedHis.setCantidadUnd(1.0);
			docPedHisService.saveDetOCPedHis(docPedHis);

			seq++;

		}

		fac.setDetalles(detalles);
		encabezadoService.saveOC(fac);

	}

	// MODIFICAR DATOS DEL PROVEEDOR TIME REPOSICION, LEAD TIME
	@GetMapping("/modificarProveedor/{timeRepo}/{leadTime}/{id}")
	public boolean modificarProveedor(@PathVariable Integer timeRepo, @PathVariable Integer leadTime,
			@PathVariable String id) {
		Marca m = marcaService.findById(id);
		m.setTimeRepo(timeRepo);
		m.setLeadTime(leadTime);
		m.setNit(m.getNit());
		marcaService.save(m);

		return true;
	}

	// CREAR UN TIPO DE REFERENCIAS (A,B,C)
	@GetMapping("/crearRefTipo/{tipo}/{dias}")
	public boolean crearRefTipo(@PathVariable String tipo, @PathVariable Integer dias) {

		ReferenciasTipo rt = new ReferenciasTipo();
		rt.setTipo(tipo);
		rt.setDias(dias);
		refeTipoService.save(rt);

		return true;
	}

	// MODIFICAR TIPO DE REFENCIA, LOS DIAS
	@GetMapping("/modificarRefTipo/{tipo}/{dias}")
	public boolean modificarRefTipo(@PathVariable String tipo, @PathVariable Integer dias) {

		ReferenciasTipo rt = refeTipoService.findByTipo(tipo);
		rt.setDias(dias);
		refeTipoService.save(rt);
		return true;
	}

	// CAMBIAR CANTIDAD DE ORDEN DE COMPRA
	@GetMapping("/cambiarDetPreOrden/{numero}/{tipo}/{cnt}/{producto}")
	public void cambiarDetPreOrden(@PathVariable Integer numero, @PathVariable String tipo, @PathVariable Integer cnt,
			@PathVariable String producto) {
		Date f = new Date();
		DetalleOCPed dop = docPedService.findByPed(numero, tipo, producto);
		Um u = umService.findByUndMin(producto, "CJ");
		if (u != null) {
			cnt = cnt * u.getConv();
			dop.setCantidadDes((double) cnt);
		} else {
			dop.setCantidadDes((double) cnt);
		}

		Object prom = detOcServices.findByPromVenta("FAC", producto, (f.getYear() + 1900), f.getMonth());
		Object count = detOcServices.findByCount("FAC", producto, (f.getYear() + 1900), f.getMonth());
		double promedio = 0;

		if (prom != null) {
			promedio = ((double) prom / (long) count);
			dop.setInvSeg((int) Math.round(Math.round(dop.getCantidadDes()) / promedio));
		} else {
			dop.setInvSeg((int) promedio);
		}

		docPedService.saveDetOCPed(dop);
	}

	// GUARDAR LOTE DE UN PRODUCTO EN UNA ORDEN DE COMPRA
	@GetMapping("/guardarLoteOrc/{tipo}/{numero}/{codigo}/{cantidad}/{lote}/{fecha}/{total}")

	public boolean guardarLoteOrc(@PathVariable String tipo, @PathVariable String numero, @PathVariable String codigo,
			@PathVariable Double cantidad, @PathVariable String lote, @PathVariable String fecha,
			@PathVariable Integer total) throws ParseException {

		boolean resulLote = false;
		boolean resulEstado = false;
		boolean resulFinal = false;

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date date = formatter.parse(fecha);

		Lote l = new Lote();
		l.setTipo(tipo);
		l.setNumero(numero);
		l.setCodigo(codigo);
		l.setCantidad(cantidad);
		l.setLote(lote);
		l.setVencimiento(date);
		Lote rl = loteOrcService.saveLote(l);
		if (rl != null) {
			resulLote = true;
		}

		DetalleOCPed det = docPedService.findByPed(Integer.parseInt(numero), tipo, codigo);
		det.setCantidadRec(total);
		det.setFechaRecibo(date);
		det.setEstado("V");
		DetalleOCPed re = docPedService.saveDetOCPed(det);

		if (re != null) {
			resulEstado = true;
		}

		if (resulLote == true && resulEstado == true) {
			resulFinal = true;
		}

		resulFinal = false;

		return resulFinal;

	}

	// CAMBIAR ESTADO CUANDO EL PRODUCTO NO LLEGO
	@GetMapping("/noLlego/{numero}/{tipo}/{codigo}")
	public void noLlego(@PathVariable String numero, @PathVariable String tipo, @PathVariable String codigo) {
		DetalleOCPed det = docPedService.findByPed(Integer.parseInt(numero), tipo, codigo);
		det.setCantidadRec(0);
		det.setEstado("V");
		docPedService.saveDetOCPed(det);
	}

	// GUARDAR EL NUMERO DE LA FACTURA, RECEPCION DE MATERIALES
	@GetMapping("/guardarFac/{numero}/{tipo}/{numFac}")
	public void guardarFac(@PathVariable String numero, @PathVariable String tipo, @PathVariable String numFac) {
		Ped p = pedService.findbyPed(Integer.parseInt(numero), tipo);
		p.setNotas(numFac);
		pedService.savePed(p);
	}
}
